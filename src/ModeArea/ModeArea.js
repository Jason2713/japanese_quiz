import React from 'react';
import ModeButton from './ModeButton';
import TEXT_TYPE from '../data/TEXT_TYPE';
import ITEM_TYPE from '../data/ITEM_TYPE'
import '../App.css';
import './ModeArea.css';

const ModeArea = (props)=>{
    //functions
    const { setTextTypeReq, setItemTypeReq, toggleQuestionMode, setChapterNo, handleModeReset } = props;
    //variables
    const { itemTypeRequest, textTypeRequest, chapterNo } = props;
    return(
        <div className='App-header'>
            <em className='fade-in'>Choose your flavours</em>
            <div className='mode-buttons'>
                
                <div className='flex-box fade-in'>
                    <button id='q-mode' onClick={ toggleQuestionMode }> Eng / Jap </button>
                </div>
                
                <div className='flex-box fade-in'>
                    <ModeButton setReqTo={setTextTypeReq} buttonType={TEXT_TYPE.HIRAGANA} selectedButtonType={textTypeRequest} buttonClass='text-type'> 
                        Hiragana 
                    </ModeButton>
                    <ModeButton setReqTo={setTextTypeReq} buttonType={TEXT_TYPE.KATAKANA} selectedButtonType={textTypeRequest} buttonClass='text-type'> 
                        Katakana 
                    </ModeButton>
                    <ModeButton setReqTo={setTextTypeReq} buttonType={TEXT_TYPE.RANDOM} selectedButtonType={textTypeRequest} buttonClass='text-type'> 
                        Random 
                    </ModeButton>
                </div>

                {/* Note: this section is made not to work deliberately. 
                Check notes in this.setItemTypeReq() */}
                <div className='flex-box fade-in'>
                    <ModeButton setReqTo={setItemTypeReq} buttonType={ITEM_TYPE.COLOR} selectedButtonType={itemTypeRequest} buttonClass='item-type'> 
                        item type 0
                    </ModeButton>
                    <ModeButton setReqTo={setItemTypeReq} buttonType={ITEM_TYPE.COMMUNICATION} selectedButtonType={itemTypeRequest} buttonClass='item-type'> 
                        item type 1
                    </ModeButton>
                    <ModeButton setReqTo={setItemTypeReq} buttonType={ITEM_TYPE.DIRECTION} selectedButtonType={itemTypeRequest} buttonClass='item-type'> 
                        item type 2
                    </ModeButton>
                </div>

                <div className='flex-box fade-in'>
                    <ModeButton setReqTo={setChapterNo} buttonType={1} selectedButtonType={chapterNo} buttonClass='chapter-num'> 
                        Chapter 1-2
                    </ModeButton>
                    <ModeButton setReqTo={setChapterNo} buttonType={2} selectedButtonType={chapterNo} buttonClass='chapter-num'> 
                        Chapter 3-4
                    </ModeButton>
                </div>

                <div className='flex-box fade-in'>
                    <button id='reset' onClick={ handleModeReset }> + Reset + </button>                        
                </div>
            </div>
        </div>
    )
}
export default ModeArea;