import React from 'react';
import Q_MODE from '../data/MODE';

const QuizArea = (props) => {
    //functions
    const { handleKeypress, updateSubmission, handleSubmission, handleNextQuestion, handleHintDisplay } = props;
    //variables
    const { qMode, currQuestion, hintDisplay, answered, answerIsCorrect, userAns } = props;
    return (
        <div>
            <div>
                {
                    qMode === Q_MODE.ENG_TO_JAP?(
                        <div>
                            <h3> What is the Japanese of: </h3>
                            <h1> { currQuestion.question } </h1>
                        </div>
                    ):(
                        <div>
                            <h3> What is the English of: </h3>
                            <h1> { currQuestion.answer } </h1>
                        </div>
                    )
                }
                <div>
                    <input 
                        id="user_input"
                        placeholder="Your answer"
                        onKeyPress={ handleKeypress }
                        onChange={ updateSubmission }
                    />
                    <button onClick={ handleSubmission }> Submit </button>
                    <button id='skip-button' onClick={ handleNextQuestion }> Skip </button>
                </div>
    
                <p> Need help? 
                    <button 
                        id='hint-button'
                        onClick={ handleHintDisplay }>
                        Hint 
                    </button> 
                </p> 
                {
                    hintDisplay?(
                        <h3> Hint: {currQuestion.hint} </h3>
                    ):null
                }
            </div>
    
            {
                // This area shows when an answer is submitted.
                answered?(
                    <div>
                        <br/>
                        {
                            answerIsCorrect?(
                                <h3> Correct! Keep it up! </h3>
                            ):(
                                <h3> Wrong. Better luck next time! </h3>
                            )
                        }
                        <div className='answer-border'>
                            <div style={{justifyContent: "space-around", display: "flex", flexDirection: "row"}}>
                                <div style={{display: "flex", flexDirection: "column"}}>
                                    <p>Your Answer</p>
                                    <p> { userAns }</p>
                                </div>
                                <div style={{display: "flex", flexDirection: "column"}}>
                                    <p>Correct Answer</p>
                                    {
                                        qMode === Q_MODE.ENG_TO_JAP?(
                                            <p> { currQuestion.answer }</p>
                                        ):(
                                            <p> { currQuestion.question }</p>
                                        )
                                    }
                                </div>
                            </div>
                            <br/>
                            <button id='next-button' onClick={ handleNextQuestion }> Next </button>
                        </div>
                    </div>
                ): null
            }
        </div>
    )
}
export default QuizArea;