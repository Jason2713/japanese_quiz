import TEXT_TYPE from './data/TEXT_TYPE';
import ITEM_TYPE from './data/ITEM_TYPE';
import Chapter1_2 from './data/chapter1_2';
import Chapter3_4 from './data/chapter3_4';


function getRandQuestion(quizNo){
    var Chapter = null;
    switch (quizNo){
        case 1: Chapter = Chapter1_2; break;
        case 2: Chapter = Chapter3_4; break;

        default: Chapter = Chapter3_4;
    }
    const q_size = Object.keys(Chapter).length;
    const q_no = Math.floor(Math.random() * q_size);

    // console.log(Quiz1[q_no]);
    return Chapter[q_no];
}
export function getQuestionOfType(textTypeReq, itemTypeReq, quizNo){
    // console.log('at getQuestionOfType', textTypeReq, itemTypeReq, quizNo);

    //trap
    if (Object.keys(TEXT_TYPE).length < textTypeReq){
        console.error('invalid request for question text type:', textTypeReq);
        return {};
    }
    if (Object.keys(ITEM_TYPE).length < itemTypeReq){
        console.error('invalid request for question item type:', itemTypeReq);
        return {};
    }

    var randQuestion;
    var itmType;
    var txtType;
    var textTypeReqOK = false;
    var itemTypeReqOK = false;
    var i=0;
    do{
        //get a random question
        randQuestion = getRandQuestion(quizNo);
        
        txtType = randQuestion.textType;
        itmType = randQuestion.itemType;
        
        textTypeReqOK = (textTypeReq === TEXT_TYPE.RANDOM || textTypeReq === txtType);
        itemTypeReqOK = (itemTypeReq === ITEM_TYPE.RANDOM || itemTypeReq === itmType);

        //safety net
        if (i > 10000){
            console.error('too many iterations during question request');
            return {};
        }
        i += 1;
    }
    while( !(itemTypeReqOK && textTypeReqOK) );

    const finalQuestion = randQuestion;
    return finalQuestion;
}