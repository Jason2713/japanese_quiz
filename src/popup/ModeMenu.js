import React from 'react';
import './popup.css';

class ModeMenu extends React.Component{
    render(){
        return(
            <div className='popup'>
                <div className='popup-border'>
                    <div className='popup-inner'>
                        {this.props.children}
                    </div>
                    <button style={{float: 'right', borderRadius: '4px'}} onClick={this.props.closePopup}> Done! </button> 
                </div>
            </div>
        )
    }
}
export default ModeMenu;