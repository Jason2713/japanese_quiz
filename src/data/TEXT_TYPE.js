const TEXT_TYPE = {
    "RANDOM": 0,
    "HIRAGANA": 1,
    "KATAKANA": 2
};
export default TEXT_TYPE;