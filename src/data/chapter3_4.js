import TEXT_TYPE from './TEXT_TYPE';
import ITEM_TYPE from './ITEM_TYPE';

const Quiz2 = [
    //Hiragana
    { answer: 'ここ', question: 'This (place)', hint: ':)', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.DIRECTION},
    { answer: 'そこ', question: 'That (place)', hint: ':)', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.DIRECTION},
    { answer: 'あそこ', question: 'That (place, far away)', hint: ':)', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.DIRECTION},
    { answer: 'どこ', question: 'Where?', hint: ':)', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.DIRECTION},
    { answer: 'こちら', question: 'This (place, politely)', hint: ':)', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.DIRECTION},
    { answer: 'そちら', question: 'That (place, politely)', hint: ':)', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.DIRECTION},
    { answer: 'あちら', question: 'That (place, far away, politely)', hint: ':)', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.DIRECTION},
    { answer: 'どちら', question: 'Where? (politely)', hint: ':)', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.DIRECTION},
    { answer: 'きょうしつ', question: 'Classroom', hint: '教室', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'しょくどう', question: 'Restaurant', hint: '食堂', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'じむしょ', question: 'Office', hint: '事務所', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'かいぎしつ', question: 'Meeting room', hint: '会議室', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'うけつけ', question: 'Service desk', hint: '受付', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'へや', question: 'Room', hint: '部屋', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'おてあらい', question: 'Toilet (polite)', hint: 'お手洗い', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'かいだん', question: 'Stairs', hint: '階段', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'おくに', question: 'Country', hint: '御国', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'かいしゃ', question: 'Office building', hint: '会社', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'うち', question: 'Home', hint: '家', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'でんわ', question: 'Telephone', hint: '電話', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'たばこ', question: 'Tobacco', hint: '', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'うりば', question: 'Selling place', hint: '売り場', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'ちか', question: 'Underground', hint: '地下', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'なんかい', question: 'Which floor?', hint: '何階', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'いくら', question: 'How much?', hint: '幾ら', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'ひゃく', question: 'Hundred', hint: '百', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'せん', question: 'Thousand', hint: '千', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'まん', question: 'Ten-thousand', hint: '万', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'すみません', question: 'Excuse me', hint: ':)', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COMMUNICATION},
    { answer: '〜でございます', question: 'is (polite)', hint: 'で御座います', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COMMUNICATION},
    { answer: '〜を見せてください', question: 'Please let me look', hint: '', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COMMUNICATION},
    { answer: '〜をください', question: 'Please give me', hint: '', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COMMUNICATION},
    { answer: 'じゃ〜', question: 'Then (let’s do something)', hint: '', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COMMUNICATION},
    { answer: 'しんおおさか', question: 'New Osaka Station', hint: '新大阪', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.UNDEFINED},

    //Katakana
    { answer: 'ロビー', question: 'Lobby', hint: '', textType: TEXT_TYPE.KATAKANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'エレベーター', question: 'Elevator', hint: '', textType: TEXT_TYPE.KATAKANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'エスカレーター', question: 'Escalator', hint: '', textType: TEXT_TYPE.KATAKANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'イタリア', question: 'Italy', hint: '', textType: TEXT_TYPE.KATAKANA, itemType: ITEM_TYPE.UNDEFINED},
    { answer: 'スイス', question: 'Swiss', hint: '', textType: TEXT_TYPE.KATAKANA, itemType: ITEM_TYPE.UNDEFINED},


    //Colors
    { answer: 'いろ', question: 'Color', hint: '色', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.OBJECTS},
    { answer: 'くろ', question: 'Black', hint: '黒', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COLOR},
    { answer: 'あお', question: 'Blue', hint: '青', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COLOR},
    { answer: 'ちゃいろ', question: 'Black', hint: '茶色', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COLOR},
    { answer: 'はいいろ', question: 'Grey', hint: '灰色', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COLOR},
    { answer: 'みどり', question: 'Green', hint: '緑', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COLOR},
    { answer: 'だいだいいろ', question: 'Orange (color)', hint: '橙ろ', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COLOR},
    { answer: 'ももいろ', question: 'Pink', hint: '桃色', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COLOR},
    { answer: 'むらさき', question: 'Purple', hint: '紫', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COLOR},
    { answer: 'あか', question: 'Red', hint: '赤', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COLOR},
    { answer: 'みずいろ', question: 'Sky blue', hint: '水色', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COLOR},
    { answer: 'しろ', question: 'White', hint: '白', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COLOR},
    { answer: 'きいろ', question: 'Yellow', hint: '黄色', textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.COLOR},

    //Fruits
    { answer: "りんご", question: "Apple", hint: "苹果", textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.FRUITS},
    { answer: "バナナ", question: "Banana", hint: ":)", textType: TEXT_TYPE.KATAKANA, itemType: ITEM_TYPE.FRUITS},
    { answer: "もも", question: "Peach", hint: "桃", textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.FRUITS},
    { answer: "ぶどう", question: "Grapes", hint: "葡萄", textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.FRUITS},
    { answer: "いちご", question: "Strawberry", hint: "苺", textType: TEXT_TYPE.HIRAGANA, itemType: ITEM_TYPE.FRUITS},
    { answer: "パイナップル", question: "Pineapple", hint: ":)", textType: TEXT_TYPE.KATAKANA, itemType: ITEM_TYPE.FRUITS},
    { answer: "ブルーベリー", question: "Blueberry", hint: ":)", textType: TEXT_TYPE.KATAKANA, itemType: ITEM_TYPE.FRUITS},
    { answer: "すいか", question: "Watermelon", hint: "西瓜", textType: TEXT_TYPE.KATAKANA, itemType: ITEM_TYPE.FRUITS},
    { answer: "さくらんば", question: "Cherry", hint: "桜んば", textType: TEXT_TYPE.KATAKANA, itemType: ITEM_TYPE.FRUITS}
]
export default Quiz2;