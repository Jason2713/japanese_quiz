const ITEM_TYPE = {
    "UNDEFINED": 0, // lazy and no time to implement? use this type.
    "OBJECTS": 1,
    "GRAMMAR": 2, // 私＿学生です
    "PLACE": 3,
    "DIRECTION": 4, //"There", "Where"
    "COMMUNICATION": 5, //"Thank you", "Excuse me?"
    "COLOR": 6,
    "FRUITS": 7
};
export default ITEM_TYPE;