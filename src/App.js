import React, {Component} from "react"; //{} means optional import. Component was not in the code base
import { getQuestionOfType } from './QuestionGetter';
import Q_MODE from './data/MODE';
import TEXT_TYPE from './data/TEXT_TYPE';
import ITEM_TYPE from './data/ITEM_TYPE';
import ModeArea from './ModeArea/ModeArea';
import QuizArea from './QuizArea/QuizArea';
import ModeMenu from './popup/ModeMenu';

import './App.css';

class App extends Component{ //App is a subclass of Component class
    state = {
        qMode: Q_MODE.ENG_TO_JAP,
        textTypeRequest: TEXT_TYPE.RANDOM,
        itemTypeRequest: ITEM_TYPE.RANDOM,
        currQuestion: null,
        userAns: "",
        answered: false,
        answerIsCorrect: false,
        hintDisplay: false,
        chapterNo: 1,
        showPopup: false
    };
    componentDidMount(){
        this.handleNextQuestion();
    }
    clearInputBar = ()=>{
        const input_bar = document.getElementById("user_input");
        if (!input_bar) return;
        input_bar.value = "";
    }
    handleNextQuestion = ()=>{
        //fetch a question
        const textTypeReq = this.state.textTypeRequest;
        const itemTypeReq = this.state.itemTypeRequest;
        const currQuestion = getQuestionOfType(textTypeReq, itemTypeReq, this.state.chapterNo);

        //update state
        this.setState({currQuestion, answered: false, userAns: "", hintDisplay: false});

        this.clearInputBar();

        // auto focus to the input bar
        const input_bar = document.getElementById("user_input");
        if (!input_bar) return;
        input_bar.focus();
    }
    updateSubmission = (event)=>{
        this.setState({userAns: event.target.value});
    }
    handleHintDisplay = ()=>{
        this.setState({hintDisplay: true});
    }
    handleSubmission = ()=>{
        // console.log("submitted");
        const usr_answer = this.state.userAns;
        if (usr_answer === "") return;

        var correct_answer;

        if (this.state.qMode === Q_MODE.ENG_TO_JAP)
            correct_answer = this.state.currQuestion.answer;
        else if (this.state.qMode === Q_MODE.JAP_TO_ENG)
            correct_answer = this.state.currQuestion.question;

        if (usr_answer === "hint?"){
            this.setState({hintDisplay: true});
            this.clearInputBar();
            return;
        }
        // cut away anything after '('
        if (correct_answer.indexOf('（') !== -1){
            correct_answer = correct_answer.toLowerCase().substring(0, correct_answer.indexOf('（'));
        }

        if (usr_answer.toLowerCase() === correct_answer.toLowerCase()){
            this.setState({answerIsCorrect: true, answered: true, hintDisplay: false});
        }
        else{
            this.setState({answerIsCorrect: false, answered: true, hintDisplay: false});
        }

        // hide mobile phone's virtual keyboard
        var field = document.createElement('input');
        field.setAttribute('type', 'text');
        document.body.appendChild(field);

        setTimeout(function() {
            field.focus();
            setTimeout(function() {
                field.setAttribute('style', 'display:none;');
            }, 50);
        }, 50);
    }
    handleKeypress = (event)=>{
        if (event.key === "Enter"){
            if (!this.state.answered)
                this.handleSubmission();
            else
                this.handleNextQuestion();
        }
    }
    toggleQuestionMode = ()=>{
        const qMode = this.state.qMode;
        if (qMode === Q_MODE.ENG_TO_JAP){
            this.setState({qMode: Q_MODE.JAP_TO_ENG, hintDisplay: false})
        }
        else this.setState({qMode: Q_MODE.ENG_TO_JAP, hintDisplay: false})
    }
    handleModeReset = ()=>{
        this.setState({
            q_mode: Q_MODE.ENG_TO_JAP, 
            textTypeRequest: TEXT_TYPE.RANDOM, 
            itemTypeRequest: ITEM_TYPE.RANDOM,
            quizNo: 1
        }, ()=>{this.handleNextQuestion()});
    }
    setTextTypeReq = (textTypeRequest)=>{
        if (textTypeRequest === this.state.textTypeRequest) return;
        
        console.log('set requested text type to', textTypeRequest);
        this.setState({textTypeRequest}, ()=>{this.handleNextQuestion()});
    }
    setItemTypeReq = (itemTypeRequest)=>{
        console.log(itemTypeRequest);
        if (itemTypeRequest === this.state.itemTypeRequest) return;

        console.log('set requested item type to', itemTypeRequest);

        /* Note: this is made not to work deliberately, since the 
        question bank is not entered with item types, YET.
        when everything is ready uncomment the following line */

        // this.setState({itemTypeRequest}, ()=>{this.handleNextQuestion()});
    }
    setChapterNo = (chapterNo)=>{
        if (chapterNo === this.state.chapterNo) return;

        console.log('set requested quiz type to', chapterNo);
        this.setState({chapterNo}, ()=>{this.handleNextQuestion()});
    }
    togglePopup = ()=>{
        this.setState({  
            showPopup: !this.state.showPopup  
        }); 
    }
    render(){
        if (!this.state.currQuestion){
            return null;
        }
        return(
            <div className='App'>
                <h1 >Japanese Practice !</h1>
                <button style={{
                    position: 'fixed',
                    width: 30,
                    height: 30,
                    top: 5, 
                    right: 5
                    }}
                    onClick={this.togglePopup }> X </button>  
            
                {
                    this.state.showPopup?
                    <ModeMenu closePopup={ this.togglePopup }>
                        <ModeArea
                            setTextTypeReq={ this.setTextTypeReq }
                            setItemTypeReq={ this.setItemTypeReq }
                            toggleQuestionMode={ this.toggleQuestionMode }
                            setChapterNo={ this.setChapterNo }
                            handleModeReset={ this.handleModeReset }
        
                            textTypeRequest={ this.state.textTypeRequest }
                            itemTypeRequest={ this.state.itemTypeRequest }
                            chapterNo={ this.state.chapterNo }
                        />
                    </ModeMenu>
                    :null
                }

                <QuizArea
                    handleKeypress={ this.handleKeypress }
                    updateSubmission={ this.updateSubmission }
                    handleSubmission={ this.handleSubmission }
                    handleNextQuestion={ this.handleNextQuestion }
                    handleHintDisplay={ this.handleHintDisplay }

                    qMode={ this.state.qMode }
                    currQuestion={ this.state.currQuestion }
                    hintDisplay={ this.state.hintDisplay }
                    answered={ this.state.answered }
                    answerIsCorrect={ this.state.answerIsCorrect }
                    userAns={ this.state.userAns }
                />
            </div>
        )
    }
}
export default App;